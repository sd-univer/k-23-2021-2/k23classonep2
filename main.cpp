#include <iostream>

class Animal {
private:
    std::string name;

public:

    void setName(std::string newName) {
        name = newName;
    }

    void greet() {
        std::cout << "Hello from " << name << std::endl;
    }
};

int main() {
    Animal ani;

    //ani.name = "Super Cat";
    ani.setName("Super Cat");
    ani.greet();

    return 0;
}
